#!/usr/bin/env python
#http://wiki.ros.org/turtlesim/Tutorials/Moving%20in%20a%20Straight%20Line#CA-e22c413483c3a71f8104cdebb87bfe28a08bcf54_1
import rospy
from geometry_msgs.msg import Twist

def move():
    # Starts a new node
    rospy.init_node('robot_cleaner', anonymous=True)
    velocity_publisher = rospy.Publisher('/mobile_base/commands/velocity', Twist, queue_size=10) 
    vel_msg = Twist()
    
    #Need to compensate for subscriber 
    rate = rospy.Rate(10) # 10hz
    
    
    #Phase 1
    rate.sleep()
    print("Moving TurtleBot forward at 0.3m/s for 5s")
    speed = 0.3
    time = 5 

    vel_msg.linear.x = abs(speed)
    #Since we are moving just in x-axis
    vel_msg.linear.y = 0
    vel_msg.linear.z = 0
    vel_msg.angular.x = 0
    vel_msg.angular.y = 0
    vel_msg.angular.z = 0

    #run for specific time 
    t0 = rospy.Time.now().to_sec()
    t1 = t0

    #Loop to move the turtle in an specified distance
    while(t1-t0 < time):
        #Publish the velocity
        velocity_publisher.publish(vel_msg)
        t1=rospy.Time.now().to_sec()
    #After the loop, stops the robot
    vel_msg.linear.x = 0
    #Force the robot to stop
    velocity_publisher.publish(vel_msg)


    
    #Phase 2
    rate.sleep()
    print("Rotating TurtleBot at 1m/s for 1s")
    speed = 1
    time = 1 

    vel_msg.linear.x = 0 
    vel_msg.linear.y = 0
    vel_msg.linear.z = 0
    #Since we are rotating just within x-axis
    vel_msg.angular.x = 0 
    vel_msg.angular.y = 0
    vel_msg.angular.z = abs(speed)
    
    #run for specific time 
    t0 = rospy.Time.now().to_sec()
    t1 = t0

    while(t1-t0 < time):
        #Publish the velocity
        velocity_publisher.publish(vel_msg)
        t1=rospy.Time.now().to_sec()
    #After the loop, stops the robot
    vel_msg.linear.x = 0
    #Force the robot to stop
    velocity_publisher.publish(vel_msg)



    #Phase 3
    rate.sleep()
    print("Moving TurtleBot forward at 0.2m/s for 5s")
    speed = 0.2
    time = 5

    vel_msg.linear.x = abs(speed)
    vel_msg.linear.y = 0
    vel_msg.linear.z = 0
    vel_msg.angular.x = 0
    vel_msg.angular.y = 0
    vel_msg.angular.z = 0
    
    #run for specific time 
    t0 = rospy.Time.now().to_sec()
    t1 = t0

    while(t1-t0 < time):
        velocity_publisher.publish(vel_msg)
        t1=rospy.Time.now().to_sec()
        #After the loop, stops the robot
    vel_msg.linear.x = 0
    #Force the robot to stop
    velocity_publisher.publish(vel_msg)




if __name__ == '__main__':
    try:
        #Testing our function
        move()
    except rospy.ROSInterruptException: pass
